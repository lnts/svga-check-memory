# svga-check-memory  

This is a check svga file memory, and other related information of a node tool.

## Installation

As a node.js module

```shell
$ npm install svga-check-memory 
```

## Examples
```
const svgaCheck = require('svga-check-memory');

svgaCheck('./xxx.svga').then(res => {
    console.log(res);
}).catch(e => {
    console.info(e);
});

return =>
{
    "success": true,
    "format_use_memory": "",
    "use_memory_bites": 0,
    "info": "",
    "movie": {
        "images": {},
        "params": {},
        "sprites": []
    }
}

OR

return => 
{
    "success": false,
    "info": "message"
} 
```


