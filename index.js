/**
 * Created by jasee on 2020/3/24
 */
const fs = require('fs');
const assignUtils = require('pako/lib/utils/common').assign;
const inflate = require("pako/lib/inflate");
const pako = {};
const protobuf = require("protobufjs/light");
const svgaDescriptor = JSON.parse(`{"nested": {"com": {"nested": {"opensource": {"nested": {"svga": {"options": {"objc_class_prefix": "SVGAProto","java_package": "com.opensource.svgaplayer.proto"},"nested": {"MovieParams": {"fields": {"viewBoxWidth": {"type": "float","id": 1},"viewBoxHeight": {"type": "float","id": 2},"fps": {"type": "int32","id": 3},"frames": {"type": "int32","id": 4}}},"SpriteEntity": {"fields": {"imageKey": {"type": "string","id": 1},"frames": {"rule": "repeated","type": "FrameEntity","id": 2}}},"AudioEntity": {"fields": {"audioKey": {"type": "string","id": 1},"startFrame": {"type": "int32","id": 2},"endFrame": {"type": "int32","id": 3},"startTime": {"type": "int32","id": 4}}},"Layout": {"fields": {"x": {"type": "float","id": 1},"y": {"type": "float","id": 2},"width": {"type": "float","id": 3},"height": {"type": "float","id": 4}}},"Transform": {"fields": {"a": {"type": "float","id": 1},"b": {"type": "float","id": 2},"c": {"type": "float","id": 3},"d": {"type": "float","id": 4},"tx": {"type": "float","id": 5},"ty": {"type": "float","id": 6}}},"ShapeEntity": {"oneofs": {"args": {"oneof": ["shape","rect","ellipse"]}},"fields": {"type": {"type": "ShapeType","id": 1},"shape": {"type": "ShapeArgs","id": 2},"rect": {"type": "RectArgs","id": 3},"ellipse": {"type": "EllipseArgs","id": 4},"styles": {"type": "ShapeStyle","id": 10},"transform": {"type": "Transform","id": 11}},"nested": {"ShapeType": {"values": {"SHAPE": 0,"RECT": 1,"ELLIPSE": 2,"KEEP": 3}},"ShapeArgs": {"fields": {"d": {"type": "string","id": 1}}},"RectArgs": {"fields": {"x": {"type": "float","id": 1},"y": {"type": "float","id": 2},"width": {"type": "float","id": 3},"height": {"type": "float","id": 4},"cornerRadius": {"type": "float","id": 5}}},"EllipseArgs": {"fields": {"x": {"type": "float","id": 1},"y": {"type": "float","id": 2},"radiusX": {"type": "float","id": 3},"radiusY": {"type": "float","id": 4}}},"ShapeStyle": {"fields": {"fill": {"type": "RGBAColor","id": 1},"stroke": {"type": "RGBAColor","id": 2},"strokeWidth": {"type": "float","id": 3},"lineCap": {"type": "LineCap","id": 4},"lineJoin": {"type": "LineJoin","id": 5},"miterLimit": {"type": "float","id": 6},"lineDashI": {"type": "float","id": 7},"lineDashII": {"type": "float","id": 8},"lineDashIII": {"type": "float","id": 9}},"nested": {"RGBAColor": {"fields": {"r": {"type": "float","id": 1},"g": {"type": "float","id": 2},"b": {"type": "float","id": 3},"a": {"type": "float","id": 4}}},"LineCap": {"values": {"LineCap_BUTT": 0,"LineCap_ROUND": 1,"LineCap_SQUARE": 2}},"LineJoin": {"values": {"LineJoin_MITER": 0,"LineJoin_ROUND": 1,"LineJoin_BEVEL": 2}}}}}},"FrameEntity": {"fields": {"alpha": {"type": "float","id": 1},"layout": {"type": "Layout","id": 2},"transform": {"type": "Transform","id": 3},"clipPath": {"type": "string","id": 4},"shapes": {"rule": "repeated","type": "ShapeEntity","id": 5}}},"MovieEntity": {"fields": {"version": {"type": "string","id": 1},"params": {"type": "MovieParams","id": 2},"images": {"keyType": "string","type": "bytes","id": 3},"sprites": {"rule": "repeated","type": "SpriteEntity","id": 4},"audios": {"rule": "repeated","type": "AudioEntity","id": 5}}}}}}}}}}}`)
const proto = protobuf.Root.fromJSON(svgaDescriptor);
const ProtoMovieEntity = proto.lookupType("com.opensource.svga.MovieEntity");
assignUtils(pako, inflate);

/**
 * @return {string}
 */
const Uint8ToString = function (u8a) {
    var CHUNK_SZ = 0x8000;
    var c = [];
    for (var i = 0; i < u8a.length; i += CHUNK_SZ) {
        c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
    }
    return c.join("");
};

function toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}

/**
 *
 * @param {*} url  svga path
 * @param {*} cb
 * @param {*} failure
 */
function parseSvga(url, cb, failure) {
    fs.readFile(url, (err, data) => {
        if (err) throw err;
        data = toArrayBuffer(data);
        _load_viaProto(data, cb, failure);
    })
}

let _load_viaProto = (arraybuffer, cb, failure) => {
    try {
        const inflatedData = pako.inflate(arraybuffer);
        const movieData = ProtoMovieEntity.decode(inflatedData);
        let images = {};
        _loadImages(images, undefined, movieData, function () {
            movieData.ver = "2.0";
            cb && cb({
                movie: movieData,
                images,
            })
        })
    } catch (err) {
        failure && failure("The svga file must be use 2.0 version. or "+err);

    }
};

let _loadImages = function (images, zip, movieData, imagesLoadedBlock) {
    if (typeof movieData === "object" && movieData.$type === ProtoMovieEntity) {
        var finished = true;
        if (!zip) {
            for (const key in movieData.images) {
                if (movieData.images.hasOwnProperty(key)) {
                    images[key] = movieData.images[key];
                }
            }
        } else {
            for (const key in movieData.images) {
                if (movieData.images.hasOwnProperty(key)) {
                    const element = movieData.images[key];
                    const value = Uint8ToString(element);
                    if (images.hasOwnProperty(key)) {
                        continue;
                    }
                    finished = false;
                    zip.file(value + ".png").async("base64").then(function (data) {
                        images[key] = data;
                        _loadImages(images, zip, movieData, imagesLoadedBlock);
                    }.bind(this));
                    break;
                }
            }
        }
        finished && imagesLoadedBlock.call(this)
    }
    else {
        var finished = true;
        for (var key in movieData.images) {
            if (movieData.images.hasOwnProperty(key)) {
                var element = movieData.images[key];
                if (images.hasOwnProperty(key)) {
                    continue;
                }
                finished = false;
                zip.file(element + ".png").async("base64").then(function (data) {
                    images[key] = data;
                    _loadImages(images, zip, movieData, imagesLoadedBlock);
                }.bind(this));
                break;
            }
        }
        finished && imagesLoadedBlock.call(this)
    }
};

/**
 *
 * @param {*} data   ArrayBuffer  return image width and height
 */
function getImageWH(data) {
    var _data = new Uint8Array(data);
    var width = _data[18] * 256 + _data[19];
    var height = _data[22] * 256 + _data[23];
    return {
        width: width,
        height: height
    };
}

function readableBytes(num) {
    let neg = num < 0;

    let units = ['B', 'KB', 'MB', 'GB', 'TB'];

    if (neg) {
        num = -num;
    }
    if (num < 1) {
        return (neg ? '-' : '') + num + ' B';
    }
    let exponent = Math.min(Math.floor(Math.log(num) / Math.log(1024)), units.length - 1);
    num = Number((num / Math.pow(1024, exponent)).toFixed(2));

    let unit = units[exponent];
    return (neg ? '-' : '') + num + ' ' + unit;
}


function fsExistsSync(path) {
    try {
        fs.accessSync(path, fs.F_OK);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * svga file src
 * @param src
 * @returns {Promise<any>}
 */
function checkSvga(src) {
    return new Promise((resolve, reject) => {
        let exist = fsExistsSync(src);
        let result = {
            success: false,
            format_use_memory: 0,
            use_memory_bites: 0,
            info: '',
            movie: {}
        };
        if (exist && /(.*).svga$/.test(src)) {
            try {
                parseSvga(src, function (data) {
                    let bites = 0;
                    result.movie = {
                        images: data.movie.images,
                        params: data.movie.params,
                        sprites: data.movie.sprites,
                    };
                    Object.values(data.images).forEach(item => {
                        let imgInfo = getImageWH(item);
                        bites += imgInfo.width * imgInfo.height * 4;
                    });
                    result.format_use_memory = readableBytes(bites);
                    result.use_memory_bites = bites;
                    result.success = true;
                    resolve(result);
                }, function (e) {
                    reject({success: false, info: e});
                })
            } catch (e) {
                reject({success: false, info: e});
            }

        } else {
            resolve({
                success: false,
                info: 'svga file path is not valid'
            });
        }
    });
}
module.exports = checkSvga;
